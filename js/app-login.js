// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js'
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";



const firebaseConfig = {
    apiKey: "AIzaSyAL2kL-szo8WMuPVDZA_eoYbBYkhpM-yWc",
    authDomain: "paginaweb-pf.firebaseapp.com",
    projectId: "paginaweb-pf",
    storageBucket: "paginaweb-pf.appspot.com",
    messagingSenderId: "579400968418",
    appId: "1:579400968418:web:cd875221c87af912ba16bb"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);
const auth = getAuth(app);

const btnLogin = document.getElementById('btnLogin');

function login() {
    const email = document.getElementById('correo').value;
    const passwd = document.getElementById('password').value;

    signInWithEmailAndPassword(auth, email, passwd).then(() => {
            alert("Se inicio sesion correctamente");
            window.location.href = "/html/inventario.html";
        })
        .catch(() => {
            alert("El correo o la contraseña son incorrectas");
        });
}

btnLogin.addEventListener('click', () => {
    login();
});